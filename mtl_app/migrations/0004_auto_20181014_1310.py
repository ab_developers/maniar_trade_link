# Generated by Django 2.1.2 on 2018-10-14 13:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mtl_app', '0003_user_user_type'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='max_length',
            field=models.CharField(help_text='Length should be in mm', max_length=200),
        ),
        migrations.AlterField(
            model_name='category',
            name='max_thickness',
            field=models.CharField(help_text='Thickness should be  in mm', max_length=200),
        ),
        migrations.AlterField(
            model_name='category',
            name='max_width',
            field=models.CharField(help_text='Width should be  in mm', max_length=200),
        ),
        migrations.AlterField(
            model_name='category',
            name='min_length',
            field=models.CharField(help_text='Length should be in mm', max_length=200),
        ),
        migrations.AlterField(
            model_name='category',
            name='min_thickness',
            field=models.CharField(help_text='Thickness should be  in mm', max_length=200),
        ),
        migrations.AlterField(
            model_name='category',
            name='min_width',
            field=models.CharField(help_text='Width should be  in mm', max_length=200),
        ),
    ]
