from django.contrib import admin
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import Customer,Employee,Company,User,Lead,Location,Category,Item
from django.contrib.auth.admin import UserAdmin
from django import forms
from django.contrib import admin
from django.utils.translation import gettext as _

class CustomUserCreationForm(UserCreationForm):
    phone = forms.CharField()

    class Meta:
        model = User
        fields = ("username","email", "phone","user_type")

    #Validationg email while creating user from admin panel
    def clean_email(self):
        email = self.cleaned_data["email"]

        if not email:
            raise forms.ValidationError("You have forgotten email!")
        
        return email
            
    
    #Validationg phone while creating user from admin panel
    def clean_phone(self):
        phone = self.cleaned_data["phone"]

        if not phone:
            raise forms.ValidationError("You have forgotten phone number!")

        return phone


class CustomUserUpdateForm(UserChangeForm):
    phone = forms.CharField()

    class Meta:
        model = User
        fields = ("username","email","phone","user_type","full_name")

class CustomUserAdmin(UserAdmin):

    form = CustomUserUpdateForm
    add_form = CustomUserCreationForm

    list_display = ('username', 'phone', 'email','user_type','full_name', 
                    'first_name','last_name','is_staff','is_active','date_joined')


    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('phone', 'first_name', 'last_name', 'email','full_name')}),
        (_('Permissions'), {'fields': ("user_type",'is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )


    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'email','phone','full_name','user_type', 'password1', 'password2')}
        ),
    )



admin.site.register(User, CustomUserAdmin)

class CustomerForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(CustomerForm, self).__init__(*args, **kwargs)

        #only provide users whose type is customer
        self.fields['user'].queryset = User.objects.filter(user_type='company')

class CustomerAdmin(admin.ModelAdmin):
    # some defines for list_display, actions etc here
    form = CustomerForm
    list_display = ('user', 'company', 'is_active', 'created_on')
admin.site.register(Customer, CustomerAdmin)


class EmployeeForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(EmployeeForm, self).__init__(*args, **kwargs)

        #only provide users whose type is customer
        self.fields['user'].queryset = User.objects.filter(user_type='employee')

class EmployeeAdmin(admin.ModelAdmin):
    
    form = EmployeeForm
    list_display = ('user', 'employee_position', 'join_date', 'created_on')
admin.site.register(Employee, EmployeeAdmin)


class CompanyAdmin(admin.ModelAdmin):
	list_display = ('company_name', 'gst_no', 'line_address_1', 'city', 'state', 'created_on')
admin.site.register(Company, CompanyAdmin)



admin.site.register(Lead)
admin.site.register(Location)
admin.site.register(Category)
admin.site.register(Item)

# Register your models here.
