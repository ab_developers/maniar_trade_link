from rest_framework import serializers
from django.contrib.auth import get_user_model
from .models import Customer,Employee,Company,User,Lead,Location,Category,Item
from rest_framework.validators import UniqueValidator

class UserSerializer(serializers.ModelSerializer):


    class Meta:
        model = get_user_model()
        username = serializers.CharField(required=True)
        email = email = serializers.EmailField(validators=[UniqueValidator(queryset=get_user_model().objects.all())])
        phone = serializers.CharField(required=True)
        full_name = serializers.CharField(required=True)
        user_type = serializers.CharField(required=True)
        password = serializers.CharField(write_only=True)
        fields=('username', 'email', 'phone', 'full_name', 'user_type','password')
    
    def create(self, validated_data):

        user = get_user_model().objects.create(
            username=validated_data['username'],
            email=validated_data['email'],
            phone=validated_data['phone'],
            full_name=validated_data['full_name'],
            user_type=validated_data['user_type']
        )
        user.set_password(validated_data['password'])
        user.save()

        return user

class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields=('company_name', 'website', 'gst_no', 'line_address_1', 'city','state','country')


class EmployeeSerializer(serializers.ModelSerializer):
  
    user = UserSerializer(required=True)

    class Meta:
        model = Employee
        fields = ('user','employee_position','join_date')

    def create(self, validated_data):
        """
        Overriding the default create method of the Model serializer.
        :param validated_data: data containing all the details of student
        :return: returns a successfully created student record
        """

        user_data = validated_data.pop('user')
        user = UserSerializer.create(UserSerializer(), validated_data=user_data)
        employee, created = Employee.objects.update_or_create(user=user,
                            join_date=validated_data.pop('join_date'),
                            employee_position=validated_data.pop('employee_position'))
        return employee

class CustomerSerializer(serializers.ModelSerializer):
  
    user = UserSerializer(required=True)
    company = CompanySerializer(required=True)
    class Meta:
        model = Customer
        fields = ('user','company')

    def create(self, validated_data):
        """
        Overriding the default create method of the Model serializer.
        :param validated_data: data containing all the details of student
        :return: returns a successfully created student record
        """

        user_data = validated_data.pop('user')
        company_data = validated_data.pop('company')

        user = UserSerializer.create(UserSerializer(), validated_data=user_data)
        company = CompanySerializer.create(CompanySerializer(), validated_data=company_data)

        customer, created = Customer.objects.update_or_create(user=user,
                            company=company)
        return customer

def required(value):
        if value is None:
            raise serializers.ValidationError('This field is required')