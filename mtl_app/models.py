from django.db import models
#from django.contrib.auth.models import User
from django.contrib.auth.models import AbstractUser
#from django.contrib.auth import get_user_model
from django.utils import timezone

# Create your models here.
class User(AbstractUser):

	USER_TYPE = (
		("employee", 'employee'),
		("company", 'company')
	)

	phone = models.CharField(unique=False,null=True, blank=True,
							 max_length=15,default="",
							 help_text="Add phone number with country code")

	user_type = models.CharField(max_length=10,choices=USER_TYPE,default="1")
	full_name = models.CharField(max_length=255,default="")

	def __str__(self):
        	return self.username
			
	
class Customer(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	company = models.ForeignKey('Company', on_delete=models.CASCADE)
	is_active = models.BooleanField(default=True)
	last_modified_on = models.DateTimeField(auto_now=True)
	created_on = models.DateTimeField(auto_now_add=True)



class Employee(models.Model):

	EMPLOYEE_POSITIONS = (
		("Sales Executive", 'Sales Executive'),
		("Sales head", 'Sales head'),
		("Sales Manager", 'Sales Manager'),
	)
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	is_admin = models.BooleanField(default=False)
	employee_position = models.CharField(max_length=20,choices=EMPLOYEE_POSITIONS)
	join_date = models.DateTimeField()
	last_modified_on = models.DateTimeField(auto_now=True)
	created_on = models.DateTimeField(auto_now_add=True)



	def __str__(self):
       		return self.employee_position	

class Company(models.Model):
	company_name = models.CharField(max_length=200)
	website = models.CharField(max_length=200)
	gst_no = models.CharField(max_length=200)
	line_address_1 = models.CharField(max_length=200)
	city = models.CharField(max_length=200)
	state = models.CharField(max_length=200)
	country = models.CharField(max_length=200)
	is_active = models.BooleanField(default=True)
	last_modified_on = models.DateTimeField(auto_now=True)
	created_on = models.DateTimeField(auto_now_add=True)

	def __str__(self):
	       		return self.company_name

class Location(models.Model):
	employee = models.ForeignKey('Employee', on_delete=models.CASCADE)
	latitude = models.CharField(max_length=200)
	longitude = models.CharField(max_length=200)
	last_modified_on = models.DateTimeField(auto_now=True)
	created_on = models.DateTimeField(auto_now_add=True)

	"""def save(self, *args, **kwargs):
		''' On save, update timestamps '''
		if not self.id:
		    self.created_on = timezone.now()
		self.last_modified_on = timezone.now()
		return super(User, self).save(*args, **kwargs)	"""

class Lead(models.Model):
	name = models.CharField(max_length=200)
	source = models.CharField(max_length=200)
	requirement = models.CharField(max_length=200)
	quantity = models.CharField(max_length=200)
	email_id = models.CharField(max_length=200)
	employee =  models.OneToOneField(Employee, on_delete=models.CASCADE)

class Category(models.Model):
	name = models.CharField(max_length=200)
	tax = models.CharField(max_length=200)
	min_length = models.CharField(max_length=200,help_text="Length should be in mm")
	max_length = models.CharField(max_length=200,help_text="Length should be in mm")
	min_width = models.CharField(max_length=200,help_text="Width should be  in mm")
	max_width = models.CharField(max_length=200,help_text="Width should be  in mm")
	max_thickness = models.CharField(max_length=200,help_text="Thickness should be  in mm")
	min_thickness = models.CharField(max_length=200,help_text="Thickness should be  in mm")
	min_quantity = models.CharField(max_length=200)

class Item(models.Model):
	code = models.CharField(max_length=200)
	name = models.CharField(max_length=200)
	item_type = models.CharField(max_length=200)
	size = models.CharField(max_length=200)
	thickness = models.CharField(max_length=200)
	category = models.OneToOneField(Category, on_delete=models.CASCADE)
	Quantity = models.CharField(max_length=200)



"""class Quotation(models.Model):
	customer = ManyToOneRelationShip(Customer)
	quantity = models.CharField(max_length=200)
	unit_rate =  = models.CharField(max_length=200)
	item = OneToOneRelationShip(Item)
	tax = models.CharField(max_length=200)
	description = models.CharField(max_length=200)
	created_by = ManyToOneRelationShip(Employee)

class Order(models.Model):
	quotation = relationToQuotationIfAny
	customer = ManyToOneRelationShip(Customer)
	quantity = models.CharField(max_length=200)
	unit_rate =  = models.CharField(max_length=200)
	item = OneToOneRelationShip(Item)
	tax = models.CharField(max_length=200)
	description = models.CharField(max_length=200)
	advance_payment = models.CharField(max_length=200)
	created_by = ManyToOneRelationShip(Employee)
	status = models.CharField(max_length=200)"""



