from django.contrib.auth import authenticate
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK,
    HTTP_201_CREATED
)
from rest_framework.response import Response
from .serializers import EmployeeSerializer,UserSerializer,CustomerSerializer

@csrf_exempt
@api_view(["POST"])
#This tell authentication is not required if not specified than it will automatically add
#Because in setting we have specified default permission authentication required
@permission_classes((AllowAny,))
def login(request):
    username = request.data.get("username")
    password = request.data.get("password")
    if username is None or password is None:
        return Response({'error': 'Please provide both username and password'},
                        status=HTTP_400_BAD_REQUEST)
    user = authenticate(username=username, password=password)
    if not user:
        return Response({'error': 'Invalid Credentials'},
                        status=HTTP_404_NOT_FOUND)
    token,_ = Token.objects.get_or_create(user=user)
    return Response({'token': token.key},
                    status=HTTP_200_OK)

@csrf_exempt
@api_view(["POST"])
@permission_classes((AllowAny,))
def logout(request):
    # simply delete the token to force a login
    request.user.auth_token.delete()
    return Response(status=HTTP_200_OK)

@csrf_exempt
@api_view(["GET"])
def user_details(request):
    data = {'username': request.user.username}
    return Response(data, status=HTTP_200_OK)



"""Required Params
if employee = user_name,email,phone,full_name,user_type,is_admin,employee_position,join_date
if company or customer = user_name,email,phone,full_name,user_type,company_name,website,gst_no,line_address_1,city,state,country"""
"""{
 "user" : {
	"username" : "employee01",
	"user_type" : "employee",
	"password" : "sanandreas",
	"email" : "employee01@gmail.com",
	"full_name" : "First Employee",
	"phone" : "9638810006"
 },
 "employee_position" : "Sales Executive",
 "join_date" : "2018-10-16 11:14:53"
}

{
    "user": {
        "username": "abdevelopers",
        "email": "abdevelopers@gmail.com",
        "phone": "9638810006",
        "full_name": "ABdevelopers",
        "user_type": "company",
        "password": "sanandreas"
    },
    "company": {
        "company_name": "abdevelopers",
        "website": "abdevelopers.com",
        "gst_no": "3534545646",
        "line_address_1": "401, Hamana Residency",
        "city": "Surat",
        "state": "Gujarat",
        "country": "India"
    }
}
"""
@csrf_exempt
@api_view(["POST"])
@permission_classes((AllowAny,))
def register(request):

        if request.data.get('user', dict()).get('user_type') == 'employee':
            #Create Employee
            serializer = EmployeeSerializer(data=request.data)
            if serializer.is_valid(raise_exception=ValueError):
                serializer.create(validated_data=request.data)
                return Response(serializer.data, status=HTTP_201_CREATED)
            return Response(serializer.error_messages,
                            status=HTTP_400_BAD_REQUEST)
        elif request.data.get('user', dict()).get('user_type') == 'company':
            #Create Employee
            serializer = CustomerSerializer(data=request.data)
            if serializer.is_valid(raise_exception=ValueError):
                serializer.create(validated_data=request.data)
                return Response(serializer.data, status=HTTP_201_CREATED)
            return Response(serializer.error_messages,
                            status=HTTP_400_BAD_REQUEST)
            print('customer')


            
        


