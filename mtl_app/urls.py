from django.urls import path
from . import views
from rest_framework_swagger.views import get_swagger_view
from rest_framework.documentation import include_docs_urls

schema_view = get_swagger_view(title='MTLs API')
urlpatterns = [
    path('login', views.login, name='login'),
    path('logout', views.logout, name='logout'),
    path('register', views.register, name='register'),

    path('user_details', views.user_details, name='user_details'),
    path(r'swagger-docs/', schema_view),
    path(r'docs/', include_docs_urls(title='MTLs API',
                                    authentication_classes=[],
                                    permission_classes=[]))
]
