from django.apps import AppConfig


class MtlAppConfig(AppConfig):
    name = 'mtl_app'
